﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactuCAT
{
    public static class LlistaFactures
    {
        public static List<string> Llista_Factures = new List<string>
        {
        };
        public static List<string> GetSearchResults(string queryString)
        {
            var normalizedQuery = queryString?.ToLower() ?? "";
            return Llista_Factures.Where(f => f.ToLowerInvariant().Contains(normalizedQuery)).ToList();
        }
    }
}
