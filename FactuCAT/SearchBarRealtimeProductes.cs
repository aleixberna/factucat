﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FactuCAT
{
    public class SearchBarRealtimeProductes : ContentPage
    {
        ListView searchProductes;

        public SearchBarRealtimeProductes()
        {
            Title = "Code Realtime SearchBar";
            Padding = 10;

            SearchBar searchBarProductes = new SearchBar
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                CancelButtonColor = Color.White,
                TextColor = Color.Black,
                PlaceholderColor = Color.White
            };

            Label label = new Label
            {
                TextColor = Color.Black,
                Text = "Type in the searchbox to filter results in realtime.",
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            searchProductes = new ListView
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill
            };

            Content = new StackLayout
            {
                Children =
                {
                    searchBarProductes,
                    label,
                    searchProductes
                }
            };

            searchBarProductes.TextChanged += OnTextChanged;

            searchProductes.ItemsSource = LlistaProductes.Productes_Tiquets;
        }
      

        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchProductes.ItemsSource = LlistaProductes.GetSearchResults(e.NewTextValue);
        }
    }
}
