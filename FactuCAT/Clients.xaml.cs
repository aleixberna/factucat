﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Clients : ContentPage
    {
        internal static List<ClientsFactucat> Clientes { get => clients; set => clients = value; }

        private static List<ClientsFactucat> clients = new List<ClientsFactucat>();
        public Clients(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchResults.ItemsSource = LlistaClients.GetSearchResults(searchBar.Text);
        }
        public Clients()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public void OnTap(object sender, ItemTappedEventArgs textoPulsado)
        {
            MainPageViewModel navegacion = new MainPageViewModel(this);
            String id_product = textoPulsado.Item.ToString();
            String[] array = id_product.Split(' ');
            int idProductSplit = Convert.ToInt32(array[0]);
            Controlador.AgregarUser(idProductSplit);
            DisplayAlert("El següent producte s'afegirà a la llista:", textoPulsado.Item.ToString(), "Afegir Producte");
            navegacion.AfegirClients();

        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void Factura(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearFactura(BindingContext));
        }
        private void CrearClient(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearClient(BindingContext));

        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaClients.GetSearchResults(e.NewTextValue);
        }
    }
}