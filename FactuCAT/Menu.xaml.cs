﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public Menu(object bindingContext)
        {
            BindingContext = bindingContext;
            InitializeComponent();
        }
        private void Factures(object sender, EventArgs e)
        {
            Controlador.ObtenirFactures();
            this.Navigation.PushModalAsync(new Factures(BindingContext));
        }
        private void Tiquets(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Tiquets(BindingContext));
        }
        private void Pressupostos(object sender, EventArgs e)
        {
            Controlador.ObtenirPressupost();
            this.Navigation.PushModalAsync(new Pressupostos(BindingContext));
        }
        private void Clients(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Clients(BindingContext));
        }
        private void Login(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Login(BindingContext));
        }
        private void CrearProducte(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearProducte(BindingContext));
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Buscarclients(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new BuscarClientFactura(BindingContext));
        }
    }
}