﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace FactuCAT
{
    class Controlador
    {
        static string cadenaConexion = @"Data Source=oracle.ilerna.com;Initial Catalog=DAM2_STREETLIGHT; User ID=DAM2_STREETLIGHT;Password=Streetlight123";
        public static int con = 3;
        public static int con1 = 3;
        public static int linia = 0;
        public static int linia1 = 0;

        public static string AgregarUsuaris(ClientsFactucat client)
        {
            string sql = "INSERT INTO CLIENT(nom , cognoms, dni, email, telefon, adresa, codipostal, ciutat, empresa  ) VALUES ('" + client.Nom + "','" + client.Cognoms + "', '" + client.Dni + "', '" + client.Email + "', " + client.Telefon + ", '" + client.Adresa + "', " + client.Codipostal + ", '" + client.Ciutat + "', '" + client.Empresa + "');";
            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static List<ClientsFactucat> ObtenirClients()
        {
            string sql = "SELECT codiclient, nom,cognoms,dni FROM CLIENT";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaClients.LlistatClients = new List<string>();
                        while (reader.Read())
                        {
                            String datos = reader.GetInt32(0) + " " + reader.GetString(1) + " " + reader.GetString(2) + " " + reader.GetString(3);
                            LlistaClients.LlistatClients.Add(datos);
                        }
                    }
                }
                con.Close();
                return null;
            }
        }
        public static List<ClientsFactucat> ObtenirProductes()
        {

            string sql = "SELECT * FROM ARTICLE";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaProductes.Productes_Tiquets = new List<string>();

                        while (reader.Read())
                        {
                            String idProducte = reader.GetInt32(0).ToString();
                            String nom = reader.GetString(1);
                            String preu = reader.GetSqlMoney(2) + "€";
                            LlistaProductes.Productes_Tiquets.Add(idProducte + "  " + nom + " " + preu);

                        }
                    }
                }
                con.Close();
                return null;
            }
        }
        public static string AgregarProductes(ProductesFactucat producte)
        {
            string sql = "INSERT INTO ARTICLE(nomarticle, idarticle, preu ) VALUES ('" + producte.NomProducte + "'," + producte.IdProducte + ",  " + producte.PreuProductes + ");";
            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static bool Login(UsersFactucat users)
        {
            bool comprovacion = false;
            string sql = "SELECT NomUser, PasswordUser FROM USUARIO WHERE NomUser = '" + users.NomUser + "' AND PasswordUser = '" + users.PasswordUser + "'";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaProductes.Productes_Tiquets = new List<string>();

                        if (reader.Read())
                        {
                            comprovacion = true;
                        }
                    }
                }
                con.Close();
                return comprovacion;
            }
        }
        public static string Registre(UsersFactucat users)
        {
            string sql = "INSERT INTO USUARIO(NomUser, CognomUser, UserUser, PasswordUser, EmailUser) VALUES ('" + users.NomUser + "','" + users.CognomUser + "', '" + users.UserUser + "', '" + users.PasswordUser + "' , '" + users.EmailUser + "');";
            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static void AgregarId(int id)
        {
            string sql = "SELECT * FROM ARTICLE WHERE idarticle = " + id + "; ";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {

                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaProductes.Productes_Tiquets = new List<string>();

                        while (reader.Read())
                        {
                            ProductesFactucat producto = new ProductesFactucat();
                            producto.IdProducte = reader.GetInt32(0);
                            producto.NomProducte = reader.GetString(1);
                            producto.PreuProductes = reader.GetDecimal(2);

                            BuscarProductes.Articulos.Add(producto);
                        }
                    }
                }
                con.Close();
            }
        }
        public static void AgregarUser(int id)
        {
            string sql = "SELECT NOM, DNI, COGNOMS, ADRESA, codiclient FROM CLIENT WHERE codiclient = " + id + ";";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaClients.LlistatClients = new List<string>();

                        while (reader.Read())
                        {
                            ClientsFactucat client = new ClientsFactucat();
                            client.Nom = reader.GetString(0);
                            client.Dni = reader.GetString(1);
                            client.Cognoms = reader.GetString(2);
                            client.Adresa = reader.GetString(3);
                            client.Codiclient = reader.GetInt32(4);

                            Clients.Clientes.Add(client);
                        }
                    }
                }
                con.Close();
            }
        }
        public static string CrearFactures(ClientsFactucat users)
        {
            con++;
            DateTime thisDay = DateTime.Today;

            string sql = "INSERT INTO FACTURA(datafactura, codiclient, preu) VALUES ('" + thisDay.ToString("d") + "'," + users.Codiclient + ",'" + users.total + "');";

            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static string CrearFactures1(ProductesFactucat productes)
        {
            DateTime thisDay = DateTime.Today;
            linia++;
            string sql = "INSERT INTO LINIAFACTURA( numerofactura, numerolinia ) VALUES (" + con + ", " + linia + ");";
            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static List<ClientsFactucat> ObtenirFactures()
        {

            string sql = "SELECT nom, datafactura, numerofactura, preu FROM CLIENT inner join FACTURA on CLIENT.codiclient = FACTURA.codiclient; ";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaFactures.Llista_Factures = new List<string>();

                        while (reader.Read())
                        {
                            String nom = reader.GetString(0);
                            String data = reader.GetString(1);
                            int numfactura = reader.GetInt32(2);
                            String preu = reader.GetString(3);
                            LlistaFactures.Llista_Factures.Add(numfactura + "  " + data + " " + nom + " " + preu);

                        }
                    }
                }
                con.Close();
                return null;
            }
        }
        public static string CrearTiquet(ProductesFactucat productes)
        {
            con++;
            DateTime thisDay = DateTime.Today;

            string sql = "INSERT INTO TIQUET(datatiquet, idarticle) VALUES ('" + thisDay.ToString("d") + "'," + productes.IdProducte + ");";

            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static string CrearPressupost(ClientsFactucat users)
        {
            con++;
            DateTime thisDay = DateTime.Today;

            string sql = "INSERT INTO PRESSUPOST(datafactura, codiclient, preu) VALUES ('" + thisDay.ToString("d") + "'," + users.Codiclient + ",'" + users.total + "');";

            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {

                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static string CrearPressupost1(ProductesFactucat productes)
        {
            DateTime thisDay = DateTime.Today;
            linia++;
            string sql = "INSERT INTO LINIAPRESSUPOST( numeropressupost, numerolinia ) VALUES (" + con1 + ", " + linia + ");";
            string mensaje = "conexion intentada";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }
            return mensaje;
        }
        public static void AgregarUserPressupost(int id)
        {
            string sql = "SELECT NOM, DNI, COGNOMS, ADRESA, codiclient FROM CLIENT WHERE codiclient = " + id + ";";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaClients.LlistatClients = new List<string>();

                        while (reader.Read())
                        {
                            ClientsFactucat client = new ClientsFactucat();
                            client.Nom = reader.GetString(0);
                            client.Dni = reader.GetString(1);
                            client.Cognoms = reader.GetString(2);
                            client.Adresa = reader.GetString(3);
                            client.Codiclient = reader.GetInt32(4);

                            Clients.Clientes.Add(client);
                        }
                    }
                }
                con.Close();
            }
        }
        public static List<ClientsFactucat> ObtenirClientsPressupost()
        {
            string sql = "SELECT codiclient, nom,cognoms,dni FROM CLIENT";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaClientsPressupost.LlistatClientsPressupost = new List<string>();
                        while (reader.Read())
                        {
                            String datos = reader.GetInt32(0) + " " + reader.GetString(1) + " " + reader.GetString(2) + " " + reader.GetString(3);
                            LlistaClientsPressupost.LlistatClientsPressupost.Add(datos);
                        }
                    }
                }
                con.Close();
                return null;
            }
        }
        public static List<ClientsFactucat> ObtenirPressupost()
        {

            string sql = "SELECT nom, datafactura, numeropressupost, preu FROM CLIENT inner join PRESSUPOST on CLIENT.codiclient = PRESSUPOST.codiclient;  ";
            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        LlistaPressupost.Llista_Pressupost = new List<string>();

                        while (reader.Read())
                        {
                            String nom = reader.GetString(0);
                            String data = reader.GetString(1);
                            int numfactura = reader.GetInt32(2);
                            String preu = reader.GetString(3);
                            LlistaPressupost.Llista_Pressupost.Add(numfactura + "  " + data + " " + nom + " " + preu);

                        }
                    }
                }
                con.Close();
                return null;
            }
        }


    }
}
