﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace FactuCAT
{
    public class ClientsFactucat
    {
        public string Nom { get; set; }
        public string Cognoms { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public int Telefon { get; set; }
        public string Adresa { get; set; }
        public int Codipostal { get; set; }
        public string Ciutat { get; set; }
        public string Empresa { get; set; }
        public int Codiclient { get; set; }
        public string total { get; set; }

    }
}
