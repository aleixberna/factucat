﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Factures : ContentPage
    {
        public Factures(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchResults.ItemsSource = LlistaFactures.GetSearchResults(searchBar.Text);

        }
        public Factures()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void CrearFactura(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearFactura(BindingContext));
        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaFactures.GetSearchResults(e.NewTextValue);
        }
    }
}