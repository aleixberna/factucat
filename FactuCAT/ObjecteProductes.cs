﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactuCAT
{
    class ProductesFactucat
    {
        public int IdProducte { get; set; }
        public string NomProducte { get; set; }
        public decimal PreuProductes { get; set; }
        
    }
}
