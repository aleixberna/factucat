﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tiquets : ContentPage
    {
        public Tiquets(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public Tiquets()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void GuardaTiquet(object sender, EventArgs e)
        {
            DisplayAlert("Factura Creada", "Aquest tiquet ha estat creat correctament", "Acceptar");
        }
        private void BuscarProductes(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new BuscarProductes(BindingContext));
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
    }
}