﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Pressupostos : ContentPage
    {
        public Pressupostos(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchResults.ItemsSource = LlistaPressupost.GetSearchResults(searchBar.Text);

        }
        public Pressupostos()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void CrearPressupostos(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearPressupost(BindingContext));
        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaPressupost.GetSearchResults(e.NewTextValue);
        }
    }
}