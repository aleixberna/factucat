﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactuCAT
{
    public class LlistaPressupost
    {
        public static List<string> Llista_Pressupost = new List<string>
        {
        };
        public static List<string> GetSearchResults(string queryString)
        {
            var normalizedQuery = queryString?.ToLower() ?? "";
            return Llista_Pressupost.Where(f => f.ToLowerInvariant().Contains(normalizedQuery)).ToList();
        }
    }
}

