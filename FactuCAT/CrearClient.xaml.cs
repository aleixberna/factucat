﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearClient : ContentPage
    {
        public CrearClient(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public CrearClient()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Clients(object sender, EventArgs e)
        {
            Controlador.ObtenirClients();
            this.Navigation.PushModalAsync(new BuscarClientFactura(BindingContext));
        }
    }
}