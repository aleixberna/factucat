﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactuCAT
{
    public static class LlistaClients
    {
        public static List<string> LlistatClients  = new List<string>
        {

            
        };
        
        public static List<string> GetSearchResults(string queryString)
        {
            var normalizedQuery = queryString?.ToLower() ?? "";
            return LlistatClients.Where(f => f.ToLowerInvariant().Contains(normalizedQuery)).ToList();
        }
    }
}
