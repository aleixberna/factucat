﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public Login()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void Registre(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Registre(BindingContext));
        }
    }
}