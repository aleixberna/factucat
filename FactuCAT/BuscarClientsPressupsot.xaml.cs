﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarClientsPressupsot : ContentPage
    {
        public AgregarClientsPressupsot(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchResults.ItemsSource = LlistaClientsPressupost.GetSearchResults(searchBar.Text);
        }
        public AgregarClientsPressupsot()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public void OnTap(object sender, ItemTappedEventArgs textoPulsado)
        {
            MainPageViewModel navegacion = new MainPageViewModel(this);
            String id_product = textoPulsado.Item.ToString();
            String[] array = id_product.Split(' ');
            int idProductSplit = Convert.ToInt32(array[0]);
            Controlador.AgregarUserPressupost(idProductSplit);
            DisplayAlert("El següent producte s'afegirà a la llista:", textoPulsado.Item.ToString(), "Afegir Producte");
            navegacion.AfegirClients();
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void Pressupost(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearPressupost(BindingContext));
        }
        private void CrearClient(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearClient(BindingContext));

        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaClients.GetSearchResults(e.NewTextValue);
        }
    }
}