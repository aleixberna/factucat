﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactuCAT
{
    public class LlistaClientsPressupost
    {
        public static List<string> LlistatClientsPressupost = new List<string>
        {


        };

        public static List<string> GetSearchResults(string queryString)
        {
            var normalizedQuery = queryString?.ToLower() ?? "";
            return LlistatClientsPressupost.Where(f => f.ToLowerInvariant().Contains(normalizedQuery)).ToList();
        }
    }
}
