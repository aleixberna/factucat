﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace FactuCAT
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");

        public ICommand CommandAgregarUsuari { get; set; }
        public ICommand CommandBuscarProductes{ get; set; }
        public ICommand CommandClientsProductes { get; set; }
        public ICommand CommandAgregarProducte { get; set; }
        public ICommand CommandLogin { get; set; }
        public ICommand CommandRegistre { get; set; }
        public ICommand CommandInsertarLiniaTiquet { get; set; }
        public ICommand CommandInsertarLiniaFactura { get; set; }
        public ICommand CommandClient { get; set; }
        public ICommand CommandCa { get; set; }
        public ICommand CommandEn { get; set; }
        public ICommand CommandEs { get; set; }
        public ICommand CommandReset { get; set; }
        public ICommand CommandFactucat { get; set; }
        public ICommand CommandPresupost{ get; set; }
        public ICommand CommandTiquet { get; set; }
        public ICommand CommandClientspressupost { get; set; }
        public ICommand CommandAfegirclinent { get; set; }
        

        ContentPage nav;
        //Variable 
        public int CodClient;
        public String nomClient;
        public String cognomClient;
        public String dniClient;
        public String emailClient;
        public String adresaClient;
        public String ciutatClient;
        public String empresaClient;
        public String nomProducte;
        public String nomUser;
        public String userUser;
        public String cognomUser;
        public String passwordUser;
        public String password2User;
        public String emailUser;
        public String nombreProducto;
        public String preuProducto;
        public String nomcodipostalClient;
        public String nomtelefonClient;
        public String fotoFactures = "ca_factures.png";
        public String fotoTiquets = "ca_tiquets.png";
        public String fotoPressupostos = "ca_pressupostos.png";
        public String fotoClients = "ca_clients.png";
        public String fotoProductes = "ca_productes.png";
        public String fotoSortir = "ca_sortir.png";

        public String tBuscarProductes = "Buscar Productes";
        public String tBuscarFactures = "Buscar Factures";
        public String tBuscarClients = "Buscar Clients";
        public String tBuscarPressupostos = "Buscar Pressupostos";
        public String tBuscarProducte = "BUSCAR PRODUCTE";
        public String tTornar = "TORNAR";
        public String tCrearClient = "CREAR CLIENT";
        public String tBuscarClient = "BUSCAR CLIENT";
        public String tNom = "Nom:";
        public String tNom2 = "Nom";
        public String tCognoms = "Cognoms:";
        public String tCognoms2 = "Cognoms:";
        public String tEmail = "Email:";
        public String tDNI = "DNI:";
        public String tDNI2 = "DNI:";
        public String tTelefon = "Telèfon:";
        public String tAdresa = "Adreça:";
        public String tAdresa2 = "Adreça:";
        public String tCodiPostal = "Codi Postal:";
        public String tCiutat = "Ciutat:";
        public String tEmpresa = "Empresa:";
        public String tID = "ID";
        public String tPreuTotal = "Preu total";
        public String tSubtotal = "Subtotal";
        public String tTotal = "Total";
        public String tIva = "Iva";
        public String tLogin = "INICIAR SESSIÓ";
        public String tRegistre = "REGISTRE";
        public String tUsuari = "Usuari";
        public String tEntrar = "ENTRAR";
        public String tContrasenya = "Contrasenya";
        public String tGuardarFactura = "GUARDAR FACTURA";
        public String tGuardarTiquet = "GUARDAR TIQUET";
        public String tGuardarPressupost = "GUARDAR PRESSUPOST";
        public String tCrearProducte = "CREAR PRODUCTE";
        public String tCrearFactura = "CREAR FACTURA";
        public String tCrearPressupost = "CREAR PRESSUPOST";
        public String tRegistrarUsuari = "REGISTRAR USUARI";
        public String tNPressupost = "NºPRESSUPOST ";
        public String tNTiquet = "NºTIQUET ";
        public bool habilitar = true;

        public int codipostalClient;
        public int telefonClient;
        public int idProducte;
        public int tiquet2 = 1;
        public int pressupost2 = 1;
        public int factura2 = 1;
        public decimal preuProducte;

        public bool Habilitar
        {
            set
            {
                if (habilitar != value)
                {
                    habilitar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Habilitar"));
                    }
                }
            }
            get { return habilitar; }
        }

        //Get Set

        public String NomClient
        {
            set
            {
                if (nomClient != value)
                {
                    nomClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NomClient"));
                    }
                }
            }
            get { return nomClient; }
        }
        public String CognomClient
        {
            set
            {
                if (cognomClient != value)
                {
                    cognomClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CognomClient"));
                    }
                }
            }
            get { return cognomClient; }
        }
        public String DniClient
        {
            set
            {
                if (dniClient != value)
                {
                    dniClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("DniClient"));
                    }
                }
            }
            get { return dniClient; }
        }
        public String EmailClient
        {
            set
            {
                if (emailClient != value)
                {
                    emailClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EmailClient"));
                    }
                }
            }
            get { return emailClient; }
        }
        public String AdresaClient
        {
            set
            {
                if (adresaClient != value)
                {
                    adresaClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("AdresaClient"));
                    }
                }
            }
            get { return adresaClient; }
        }
        public String CiutatClient
        {
            set
            {
                if (ciutatClient != value)
                {
                    ciutatClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CiutatClient"));
                    }
                }
            }
            get { return ciutatClient; }
        }
        public String EmpresaClient
        {
            set
            {
                if (empresaClient != value)
                {
                    empresaClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EmpresaClient"));
                    }
                }
            }
            get { return empresaClient; }
        }
        public String CodipostalClient
        {
            set
            {
                if (nomcodipostalClient != value) { nomcodipostalClient = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("CodipostalClient")); } }
            }
            get { return nomcodipostalClient; }
        }
        public String TelefonClient
        {
            set
            {
                if (nomtelefonClient != value) { nomtelefonClient = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("TelefonClient")); } }
            }
            get { return nomtelefonClient; }
        }
        public String FotoFactures
        {
            set
            {
                if (fotoFactures != value)
                {
                    fotoFactures = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoFactures"));
                    }
                }
            }
            get { return fotoFactures; }
        }
        public String NomProducte
        {
            set
            {
                if (nomProducte != value)
                {
                    nomProducte = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NomProducte"));
                    }
                }
            }
            get { return nomProducte; }
        }
        public String IdProducte
        {
            set
            {
                if (nombreProducto != value) { nombreProducto = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("IdProducte")); } }
            }
            get { return nombreProducto; }
        }
        public String PreuProducte
        {
            set
            {
                if (preuProducto != value) { preuProducto = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuProducte")); } }
            }
            get { return preuProducto; }
        }
        public String NomUser
        {
            set
            {
                if (nomUser != value)
                {
                    nomUser = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NomUser"));
                    }
                }
            }
            get { return nomUser; }
        }
        public String CognomUser
        {
            set
            {
                if (cognomUser != value)
                {
                    cognomUser = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("CognomUser"));
                    }
                }
            }
            get { return cognomUser; }
        }
        public String UserUser
        {
            set
            {
                if (userUser != value)
                {
                    userUser = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("UserUser"));
                    }
                }
            }
            get { return userUser; }
        }
        public String PasswordUser
        {
            set
            {
                if (passwordUser != value)
                {
                    passwordUser = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PasswordUser"));
                    }
                }
            }
            get { return passwordUser; }
        }
        public String Password2User
        {
            set
            {
                if (password2User != value)
                {
                    password2User = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Password2User"));
                    }
                }
            }
            get { return password2User; }
        }
        public String EmailUser
        {
            set
            {
                if (emailUser != value)
                {
                    emailUser = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EmailUser"));
                    }
                }
            }
            get { return emailUser; }
        }
        public String FotoTiquets
        {
            set
            {
                if (fotoTiquets != value)
                {
                    fotoTiquets = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoTiquets"));
                    }
                }
            }
            get { return fotoTiquets; }
        }
        public String FotoPressupostos
        {
            set
            {
                if (fotoPressupostos != value)
                {
                    fotoPressupostos = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoPressupostos"));
                    }
                }
            }
            get { return fotoPressupostos; }
        }
        public String FotoClients
        {
            set
            {
                if (fotoClients != value)
                {
                    fotoClients = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoClients"));
                    }
                }
            }
            get { return fotoClients; }
        }
        public String FotoProductes
        {
            set
            {
                if (fotoProductes != value)
                {
                    fotoProductes = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoProductes"));
                    }
                }
            }
            get { return fotoProductes; }
        }
        public String FotoSortir
        {
            set
            {
                if (fotoSortir != value)
                {
                    fotoSortir = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("FotoSortir"));
                    }
                }
            }
            get { return fotoSortir; }
        }
        public String TBuscarProductes
        {
            set
            {
                if (tBuscarProductes != value)
                {
                    tBuscarProductes = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarProductes"));
                    }
}
            }
            get { return tBuscarProductes; }
        }
        public String TBuscarFactures
        {
            set
            {
                if (tBuscarFactures != value)
                {
                    tBuscarFactures = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarFactures"));
                    }
                }
            }
            get { return tBuscarFactures; }
        }
        public String TBuscarClients
        {
            set
            {
                if (tBuscarClients != value)
                {
                    tBuscarClients = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarClients"));
                    }
                }
            }
            get { return tBuscarClients; }
        }
        public String TBuscarPressupostos
        {
            set
            {
                if (tBuscarPressupostos != value)
                {
                    tBuscarPressupostos = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarPressupostos"));
                    }
                }
            }
            get { return tBuscarPressupostos; }
        }
        public String TBuscarProducte
        {
            set
            {
                if (tBuscarProducte != value)
                {
                    tBuscarProducte = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarProducte"));
                    }
                }
            }
            get { return tBuscarProducte; }
        }
        public String TTornar
        {
            set
            {
                if (tTornar != value)
                {
                    tTornar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TTornar"));
                    }
                }
            }
            get { return tTornar; }
        }
        public String TCrearClient
        {
            set
            {
                if (tCrearClient != value)
                {
                    tCrearClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCrearClient"));
                    }
                }
            }
            get { return tCrearClient; }
        }
        public String TBuscarClient
        {
            set
            {
                if (tBuscarClient != value)
                {
                    tBuscarClient = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TBuscarClient"));
                    }
                }
            }
            get { return tBuscarClient; }
        }
        public String TNom
        {
            set
            {
                if (tNom != value)
                {
                    tNom = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TNom"));
                    }
                }
            }
            get { return tNom; }
        }
        public String TNom2
        {
            set
            {
                if (tNom2 != value)
                {
                    tNom2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TNom2"));
                    }
                }
            }
            get { return tNom2; }
        }
        public String TCognoms
        {
            set
            {
                if (tCognoms != value)
                {
                    tCognoms = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCognoms"));
                    }
                }
            }
            get { return tCognoms; }
        }
        public String TCognoms2
        {
            set
            {
                if (tCognoms2 != value)
                {
                    tCognoms2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCognoms2"));
                    }
                }
            }
            get { return tCognoms2; }
        }
        public String TEmail
        {
            set
            {
                if (tEmail != value)
                {
                    tEmail = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TEmail"));
                    }
                }
            }
            get { return tEmail; }
        }
        public String TDNI
        {
            set
            {
                if (tDNI != value)
                {
                    tDNI = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TDNI"));
                    }
                }
            }
            get { return tDNI; }
        }
        public String TDNI2
        {
            set
            {
                if (tDNI2 != value)
                {
                    tDNI2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TDNI2"));
                    }
                }
            }
            get { return tDNI2; }
        }
        public String TTelefon
        {
            set
            {
                if (tTelefon != value)
                {
                    tTelefon = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TTelefon"));
                    }
                }
            }
            get { return tTelefon; }
        }
        public String TAdresa
        {
            set
            {
                if (tAdresa != value)
                {
                    tAdresa = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TAdresa"));
                    }
                }
            }
            get { return tAdresa; }
        }
        public String TAdresa2
        {
            set
            {
                if (tAdresa2 != value)
                {
                    tAdresa2 = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TAdresa2"));
                    }
                }
            }
            get { return tAdresa2; }
        }
        public String TCodiPostal
        {
            set
            {
                if (tCodiPostal != value)
                {
                    tCodiPostal = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCodiPostal"));
                    }
                }
            }
            get { return tCodiPostal; }
        }
        public String TCiutat
        {
            set
            {
                if (tCiutat != value)
                {
                    tCiutat = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCiutat"));
                    }
                }
            }
            get { return tCiutat; }
        }
        public String TEmpresa
        {
            set
            {
                if (tEmpresa != value)
                {
                    tEmpresa = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TEmpresa"));
                    }
                }
            }
            get { return tEmpresa; }
        }
        public String TID
        {
            set
            {
                if (tID != value)
                {
                    tID = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TID"));
                    }
                }
            }
            get { return tID; }
        }
        public String TPreuTotal
        {
            set
            {
                if (tPreuTotal != value)
                {
                    tPreuTotal = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TPreuTotal"));
                    }
                }
            }
            get { return tPreuTotal; }
        }
        public String TSubtotal
        {
            set
            {
                if (tSubtotal != value)
                {
                    tSubtotal = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TSubtotal"));
                    }
                }
            }
            get { return tSubtotal; }
        }

        public String TTotal
        {
            set
            {
                if (tTotal != value)
                {
                    tTotal = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TTotal"));
                    }
                }
            }
            get { return tTotal; }
        }
        public String TIva
        {
            set
            {
                if (tIva != value)
                {
                    tIva = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TIva"));
                    }
                }
            }
            get { return tIva; }
        }
        public String TLogin
        {
            set
            {
                if (tLogin != value)
                {
                    tLogin = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TLogin"));
                    }
                }
            }
            get { return tLogin; }
        }
        public String TRegistre
        {
            set
            {
                if (tRegistre != value)
                {
                    tRegistre = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TRegistre"));
                    }
                }
            }
            get { return tRegistre; }
        }
        public String TUsuari
        {
            set
            {
                if (tUsuari != value)
                {
                    tUsuari = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TUsuari"));
                    }
                }
            }
            get { return tUsuari; }
        }
        public String TEntrar
        {
            set
            {
                if (tEntrar != value)
                {
                    tEntrar = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TEntrar"));
                    }
                }
            }
            get { return tEntrar; }
        }
        public String TContrasenya
        {
            set
            {
                if (tContrasenya != value)
                {
                    tContrasenya = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TContrasenya"));
                    }
                }
            }
            get { return tContrasenya; }
        }
        public String TGuardarFactura
        {
            set
            {
                if (tGuardarFactura != value)
                {
                    tGuardarFactura = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TGuardarFactura"));
                    }
                }
            }
            get { return tGuardarFactura; }
        }
        public String TGuardarTiquet
        {
            set
            {
                if (tGuardarTiquet != value)
                {
                    tGuardarTiquet = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TGuardarTiquet"));
                    }
                }
            }
            get { return tGuardarTiquet; }
        }
        public String TGuardarPressupost
        {
            set
            {
                if (tGuardarPressupost != value)
                {
                    tGuardarPressupost = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TGuardarPressupost"));
                    }
                }
            }
            get { return tGuardarPressupost; }
        }
        public String TCrearProducte
        {
            set
            {
                if (tCrearProducte != value)
                {
                    tCrearProducte = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCrearProducte"));
                    }
                }
            }
            get { return tCrearProducte; }
        }
        public String TCrearFactura
        {
            set
            {
                if (tCrearFactura != value)
                {
                    tCrearFactura = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCrearFactura"));
                    }
                }
            }
            get { return tCrearFactura; }
        }
        public String TCrearPressupost
        {
            set
            {
                if (tCrearPressupost != value)
                {
                    tCrearPressupost = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TCrearPressupost"));
                    }
                }
            }
            get { return tCrearPressupost; }
        }
        public String TRegistrarUsuari
        {
            set
            {
                if (tRegistrarUsuari != value)
                {
                    tRegistrarUsuari = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TRegistrarUsuari"));
                    }
                }
            }
            get { return tRegistrarUsuari; }
        }
        public String TNPressupost
        {
            set
            {
                if (tNPressupost != value)
                {
                    tNPressupost = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TNPressupost"));
                    }
                }
            }
            get { return tNPressupost; }
        }
        public String TNTiquet
        {
            set
            {
                if (tNTiquet != value)
                {
                    tNTiquet = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TNTiquet"));
                    }
                }
            }
            get { return tNTiquet; }
        }

        public double preu = 0;
        public double iva = 1.21;
        public double ivasubtotal ;
        public string ivas;
        public double subtotal;
        public string subtotals;
        public double total;
        public string totals;
        public String Total
        {
            set
            {
                if (totals != value)
                {
                    totals = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Totals"));
                    }
                }
            }
            get { return totals; }
        }
        public String Iva
        {
            set
            {
                if (ivas != value)
                {
                    ivas = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Ivas"));
                    }
                }
            }
            get { return ivas; }
        }
        public String Subtotal
        {
            set
            {
                if (subtotals != value)
                {
                    subtotals = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Subtotals"));
                    }
                }
            }
            get { return subtotals; }
        }


        public MainPageViewModel(ContentPage navi)
        {
            nav = navi;
            // ProvaComand = new Command(ProvaFuncio);
            CommandAgregarUsuari = new Command(CrearUsuarisAsync);
            CommandBuscarProductes = new Command(SelectProductesLlista);
            CommandClientsProductes = new Command(SelectClients);
            CommandClientspressupost = new Command(SelectclientPressupost);
            CommandAgregarProducte = new Command(CrearProductes);
            CommandLogin = new Command(Login);
            CommandRegistre = new Command(RegistreUsers);
            CommandInsertarLiniaTiquet = new Command(InsertarLiniaTiquet);
            CommandInsertarLiniaFactura = new Command(InsertarLiniaFactura);
            CommandClient = new Command(AfegirClients);
            CommandCa = new Command(IdiomaCa);
            CommandEn = new Command(IdiomaEn);
            CommandEs = new Command(IdiomaEs);
            CommandReset = new Command(Reset);
            CommandFactucat = new Command(CrearFactura);
            CommandPresupost = new Command(CrearPresupost);
            CommandTiquet = new Command(CrearTiquet);
            CommandAfegirclinent = new Command(AfegirClientsPressupost);
        }
        //public LoginPage()
        //{
        //    var vm = new LoginViewModel();
        //    this.BindingContext = vm;
        //    vm.DisplayInvalidLoginPrompt += () => DisplayAlert("Error", "Invalid Login, try again", "OK");
        //    InitializeComponent();

        //    Email.Completed += (object sender, EventArgs e) =>
        //    {
        //        Password.Focus();
        //    };

        //    Password.Completed += (object sender, EventArgs e) =>
        //    {
        //        vm.SubmitCommand.Execute(null);
        //    };
        //}
      
        public void CrearUsuarisAsync()
        {
            ClientsFactucat client = new ClientsFactucat();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(NomClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Nom", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(CognomClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Cognom", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(DniClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir un DNI", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(AdresaClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Adreça", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(TelefonClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Adreça", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(CodipostalClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Adreça", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(CiutatClient))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Ciutat", "Continuar");
                bol = false;
            }

            client.Nom = NomClient;
            client.Cognoms = CognomClient;
            client.Dni = DniClient;
            client.Email = EmailClient;
            client.Telefon = telefonClient;
            client.Adresa = AdresaClient;
            client.Codipostal = codipostalClient;
            client.Ciutat = CiutatClient;
            client.Empresa = EmpresaClient;
            if (bol == true)
            {
                Controlador.AgregarUsuaris(client);
                Application.Current.MainPage.DisplayAlert("Crear client:", "Client creat correctament!", "Continuar");
            }
        }

        public void SelectProductesLlista()
        {
            Controlador.ObtenirProductes();
        }
        public void SelectClients()
        {
            Controlador.ObtenirClients();
        }
        public void CrearProductes()
        {
            ProductesFactucat producte = new ProductesFactucat();
            Boolean bol = true;
            
            if (String.IsNullOrWhiteSpace(PreuProducte))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Preu", "Continuar");
                bol = false;
            }
            idProducte = Convert.ToInt32(IdProducte);
            preuProducte = Convert.ToDecimal(PreuProducte);
            producte.IdProducte = idProducte;
            producte.NomProducte = NomProducte;
            producte.PreuProductes = preuProducte;

            Controlador.AgregarProductes(producte);
            Application.Current.MainPage.DisplayAlert("Crear producte:", "Producte creat correctament!", "Continuar");
        }
       
        public void Login()
        {
            bool resultado = false;
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(NomUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Nom", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(PasswordUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Contraseña", "Continuar");
                bol = false;
            }
            UsersFactucat users = new UsersFactucat();
            users.NomUser = NomUser;
            users.PasswordUser = PasswordUser;
            resultado = Controlador.Login(users);

            if (resultado) {
                if (NomUser.Equals("admin"))
                {
                    Habilitar = true;
                }
                else
                { 
                    Habilitar = false;

                }
                NavegacioAsync();
            }
            else{
                Application.Current.MainPage.DisplayAlert("Error:", "Usuari i Contrasenya erronis", "Continuar");
            }
        }

        public async System.Threading.Tasks.Task NavegacioAsync()
        {
            await nav.Navigation.PushModalAsync(new Menu(this));
        }
        public void RegistreUsers()
        {
            UsersFactucat users = new UsersFactucat();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(NomUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Nom", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(CognomUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir el Cognom", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(UserUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir un Usuario", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(PasswordUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Contraseña", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(Password2User))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Contraseña", "Continuar");
                bol = false;
            }
            else if (String.IsNullOrWhiteSpace(EmailUser))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Has d'introduir una Email", "Continuar");
                bol = false;
            }
            else if (!PasswordUser.Equals(Password2User))
            {
                Application.Current.MainPage.DisplayAlert("Error:", "Les contrasenyes son diferents", "Continuar");
                bol = false;
            }
            users.NomUser = NomUser;
            users.CognomUser = CognomUser;
            users.UserUser = UserUser;
            users.PasswordUser = PasswordUser;
            users.Password2User = Password2User;
            users.EmailUser = EmailUser;
            if (bol == true)
            {
                Controlador.Registre(users);
                NavegacioLogin();
            }
           
        }
        public async System.Threading.Tasks.Task NavegacioLogin()
        {
            await nav.Navigation.PushModalAsync(new Login());
        }
        public void InsertarLiniaTiquet()
        {
            String IdProducte2 = " ";
            String NomProducte2 = " ";
            String PreuProducte2 = " ";

            foreach (ProductesFactucat articulo in BuscarProductes.Articulos) {
                IdProducte2 = articulo.IdProducte.ToString();                
                NomProducte2 = articulo.NomProducte;                
                PreuProducte2 = articulo.PreuProductes.ToString();
            }

            IdProducte = IdProducte + IdProducte2 +"\n";
            NomProducte = NomProducte + NomProducte2 + "\n";
            PreuProducte = PreuProducte + PreuProducte2 + "\n";
            if (!PreuProducte2.Equals(" ") )
            {
                preu = preu + Convert.ToDouble(PreuProducte2);
            }
            else
            {
                preu = 0;
            }

            subtotal = preu;
            total = subtotal * iva;
            ivasubtotal = subtotal * 0.21;

            Subtotal = subtotal.ToString();
            Iva = ivasubtotal.ToString();
            Total = total.ToString();
        }
        public void InsertarLiniaFactura()
        {
            String IdProducte2 = " ";
            String NomProducte2 = " ";
            String PreuProducte2 = " ";

            foreach (ProductesFactucat articulo in BuscarProductes.Articulos)
            {
                IdProducte2 = articulo.IdProducte.ToString();
                NomProducte2 = articulo.NomProducte;
                PreuProducte2 = articulo.PreuProductes.ToString();
            }

            IdProducte = IdProducte + IdProducte2 + "\n";
            NomProducte = NomProducte + NomProducte2 + "\n";
            PreuProducte = PreuProducte + PreuProducte2 + "\n";

            if (!PreuProducte2.Equals(" "))
            {
                preu = preu + Convert.ToDouble(PreuProducte2);
            }
            else
            {
                preu = 0;
            }
            subtotal = preu;
            total = subtotal * iva;
            ivasubtotal = subtotal * 0.21;

            Subtotal = subtotal.ToString();
            Iva = ivasubtotal.ToString();
            Total = total.ToString();
        }
        public void AfegirClients()
        {
            foreach (ClientsFactucat client in Clients.Clientes)
            {
                NomClient = client.Nom;
                DniClient = client.Dni;
                CognomClient = client.Cognoms;
                AdresaClient = client.Adresa;
                CodClient = client.Codiclient;
            }
        }

        public void IdiomaCa()
        {
            FotoFactures = "ca_factures.png";
            FotoTiquets = "ca_tiquets.png";
            FotoPressupostos = "ca_pressupostos.png";
            FotoClients = "ca_clients.png";
            FotoProductes = "ca_productes.png";
            FotoSortir = "ca_sortir.png";

            TBuscarProductes = "Buscar Productes";
            TBuscarFactures = "Buscar Factures";
            TBuscarClients = "Buscar Clients";
            TBuscarPressupostos = "Buscar Pressupostos";
            TBuscarProducte = "BUSCAR PRODUCTE";
            TTornar = "TORNAR";
            TCrearClient = "CREAR CLIENT";
            TBuscarClient = "BUSCAR CLIENT";
            TNom = "Nom:";
            TNom2 = "Nom";
            TCognoms = "Cognoms:";
            TCognoms2 = "Cognoms:";
            TEmail = "Email:";
            TDNI = "DNI:";
            TDNI2 = "DNI:";
            TTelefon = "Telèfon:";
            TAdresa = "Adreça:";
            TAdresa2 = "Adreça:";
            TCodiPostal = "Codi Postal:";
            TCiutat = "Ciutat:";
            TEmpresa = "Empresa:";
            TID = "ID";
            TPreuTotal = "Preu total";
            TSubtotal = "Subtotal";
            TTotal = "Total";
            TIva = "Iva";
            TLogin = "INICIAR SESSIÓ";
            TRegistre = "REGISTRE";
            TUsuari = "Usuari";
            TEntrar = "ENTRAR";
            TContrasenya = "Contrasenya";
            TGuardarFactura = "GUARDAR FACTURA";
            TGuardarTiquet = "GUARDAR TIQUET";
            TGuardarPressupost = "GUARDAR PRESSUPOST";
            TCrearProducte = "CREAR PRODUCTE";
            TCrearFactura = "CREAR FACTURA";
            TCrearPressupost = "CREAR PRESSUPOST";
            TRegistrarUsuari = "REGISTRAR USUARI";
            TNPressupost = "NºPRESSUPOST";
            TNTiquet = "NºTIQUET";



        }
        public void IdiomaEn()
        {
            FotoFactures = "en_factures.png";
            FotoTiquets = "en_tiquets.png";
            FotoPressupostos = "en_pressupostos.png";
            FotoClients = "en_clients.png";
            FotoProductes = "en_productes.png";
            FotoSortir = "en_sortir.png";

            TBuscarProductes = "Search Products";
            TBuscarFactures = "Search Invoices";
            TBuscarClients = "Search Clients";
            TBuscarPressupostos = "Search Budgets";
            TBuscarProducte = "SEARCH PRODUCT";
            TTornar = "BACK";
            TCrearClient = "CREATE CLIENT";
            TBuscarClient = "SEARCH CLIENT";
            TNom = "Name:";
            TNom2 = "Name";
            TCognoms = "Surname:";
            TCognoms2 = "Surname:";
            TEmail = "Email:";
            TDNI = "DNI:";
            TDNI2 = "DNI:";
            TTelefon = "Phone number:";
            TAdresa = "Address:";
            TAdresa2 = "Address:";
            TCodiPostal = "Postal code:";
            TCiutat = "City:";
            TEmpresa = "Company:";
            TID = "ID";
            TPreuTotal = "Total price";
            TSubtotal = "Subtotal";
            TTotal = "Total";
            TIva = "IVA";
            TLogin = "LOGIN";
            TRegistre = "CREATE ACCOUNT";
            TUsuari = "User";
            TEntrar = "ENTER";
            TContrasenya = "Password";
            TGuardarFactura = "SAVE INVOICE";
            TGuardarTiquet = "SAVE TICKET";
            TGuardarPressupost = "SAVE BUDGET";
            TCrearProducte = "CREATE PRODUCT";
            TCrearFactura = "CREATE INVOICE";
            TCrearPressupost = "CREATE BUDGET";
            TRegistrarUsuari = "CREATE USER";
            TNPressupost = "BUDGET Nº";
            TNTiquet = "TICKET Nº";


        }
        public void IdiomaEs()
        {
            FotoFactures = "es_factures.png";
            FotoTiquets = "es_tiquets.png";
            FotoPressupostos = "es_pressupostos.png";
            FotoClients = "es_clients.png";
            FotoProductes = "es_productes.png";
            FotoSortir = "es_sortir.png";

            TBuscarProductes = "Buscar Productos";
            TBuscarFactures = "Buscar Facturas";
            TBuscarClients = "Buscar Clientes";
            TBuscarPressupostos = "Buscar Presupuestos";
            TBuscarProducte = "BUSCAR PRODUCTO";
            TTornar = "VOLVER";
            TCrearClient = "CREAR CLIENTE";
            TBuscarClient = "BUSCARAT CLIENTE";
            TNom = "Nombre:";
            TNom2 = "Nombre";
            TCognoms = "Apellido:";
            TCognoms2 = "Apellido:";
            TEmail = "Correo:";
            TDNI = "DNI:";
            TDNI2 = "DNI:";
            TTelefon = "Teléfono:";
            TAdresa = "Dirección:";
            TAdresa2 = "Dirección:";
            TCodiPostal = "Código Postal:";
            TCiutat = "Ciudad:";
            TEmpresa = "Empresa:";
            TID = "ID";
            TPreuTotal = "Precio total";
            TSubtotal = "Subtotal";
            TTotal = "Total";
            TIva = "Iva";
            TLogin = "INICIAR SESIÓN";
            TRegistre = "REGISTRARSE";
            TUsuari = "Usuario";
            TEntrar = "ENTRAR";
            TContrasenya = "Contraseña";
            TGuardarFactura = "GUARDAR FACTURA";
            TGuardarTiquet = "GUARDAR TIQUE";
            TGuardarPressupost = "GUARDAR PRESUPUESTO";
            TCrearProducte = "CREAR PRODUCTO";
            TCrearFactura = "CREAR FACTURA";
            TCrearPressupost = "CREAR PRESUPUESTO";
            TRegistrarUsuari = "REGISTRAR USUARIO";
            TNPressupost = "NºPRESUPUESTO";
            TNTiquet = "NºTIQUE";



        }

        public void CrearFactura()
        {

            ClientsFactucat users = new ClientsFactucat();
            ProductesFactucat product = new ProductesFactucat();
            users.Codiclient = Convert.ToInt32(CodClient);
            product.IdProducte = 1;

          
            users.total = Total + " €";
            Controlador.CrearFactures(users);
            Reset();
            //Controlador.CrearFactures1(product);
        }
        public void AfegirClientsPressupost()
        {
            foreach (ClientsFactucat client in Clients.Clientes)
            {
                NomClient = client.Nom;
                DniClient = client.Dni;
                CognomClient = client.Cognoms;
                AdresaClient = client.Adresa;
                CodClient = client.Codiclient;
            }
        }
        public void CrearPresupost()
        {
            ClientsFactucat users = new ClientsFactucat();
            ProductesFactucat product = new ProductesFactucat();
            users.Codiclient = Convert.ToInt32(CodClient);
            product.IdProducte = 1;


            users.total = Total + " €";
            Controlador.CrearPressupost(users);
            Reset();
            //Controlador.CrearPressupost1(product);
        }
        public void SelectclientPressupost()
        {
            Controlador.ObtenirClientsPressupost();
        }
        public void CrearTiquet()
        {
            ProductesFactucat product = new ProductesFactucat();
            product.IdProducte = 1;
            Reset();
            Controlador.CrearTiquet(product);
        }
        public void  Reset()
        {
            IdProducte = "";
            NomProducte = "";
            PreuProducte = "";
            Subtotal = "0";
            Iva = "0";
            Total = "0";
            NomClient = "";
            CognomClient = "";
            DniClient = "";
            AdresaClient = ""; 
            preu = 0;
        }
    }
}