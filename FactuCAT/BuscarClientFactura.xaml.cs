﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BuscarClientFactura : ContentPage
    {
        public BuscarClientFactura(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchResults.ItemsSource = LlistaClients.GetSearchResults(searchBar.Text);
        }
        public BuscarClientFactura()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public void OnTap(object sender, ItemTappedEventArgs textoPulsado)
        {
            DisplayAlert("Aquest es el client seleccionat:", textoPulsado.Item.ToString(), "Acceptar"); 
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void Factura(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearFactura(BindingContext));
        }
        private void CrearClient(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearClient(BindingContext));

        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaClients.GetSearchResults(e.NewTextValue);
        }
    }
}