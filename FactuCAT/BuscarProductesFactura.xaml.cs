﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BuscarProductesFactura : ContentPage
    {
        internal static List<ProductesFactucat> Articulos { get => articulos; set => articulos = value; }

        private static List<ProductesFactucat> articulos = new List<ProductesFactucat>();
        public BuscarProductesFactura(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
            searchProductes.ItemsSource = LlistaProductes.GetSearchResults(searchBarProductes.Text);

        }
        public void OnTap(object sender, ItemTappedEventArgs textoPulsado)
        {
            MainPageViewModel navegacion = new MainPageViewModel(this);
            String id_product = textoPulsado.Item.ToString();
            String[] array = id_product.Split(' ');
            int idProductSplit = Convert.ToInt32(array[0]);
            Controlador.AgregarId(idProductSplit);
            DisplayAlert("El següent producte s'afegirà a la llista:", textoPulsado.Item.ToString(), "Afegir Producte");
            navegacion.InsertarLiniaTiquet();
            

        }
        public BuscarProductesFactura()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        public void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchProductes.ItemsSource = LlistaProductes.GetSearchResults(e.NewTextValue);
        }
        private void Factura(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CrearFactura(BindingContext));
        }
    }
}