﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FactuCAT
{
    public class SearchBarRealtimeClients : ContentPage
    {
        ListView searchResults;

        public SearchBarRealtimeClients()
        {
            Title = "Code Realtime SearchBar";
            Padding = 10;

            SearchBar searchBar = new SearchBar
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                CancelButtonColor = Color.Orange,
                PlaceholderColor = Color.Orange
            };

            Label label = new Label
            {
                Text = "Type in the searchbox to filter results in realtime.",
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            searchResults = new ListView
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill
            };

            Content = new StackLayout
            {
                Children =
                {
                    searchBar,
                    label,
                    searchResults
                }
            };

            searchBar.TextChanged += OnTextChanged;
            
            searchResults.ItemsSource = LlistaClients.LlistatClients;
        }
      

        void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            searchResults.ItemsSource = LlistaClients.GetSearchResults(e.NewTextValue);
        }
    }
}
