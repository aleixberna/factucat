﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearFactura : ContentPage
    {
        public CrearFactura(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public CrearFactura()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Factures(object sender, EventArgs e)
        {
            Controlador.ObtenirFactures();
            this.Navigation.PushModalAsync(new Factures(BindingContext));
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
            
        }
        private void BuscarProductes(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new BuscarProductesFactura(BindingContext));
        }
        private void BuscarClients(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Clients(BindingContext));
        }

    }
}