﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FactuCAT
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearPressupost : ContentPage
    {
        public CrearPressupost(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        public CrearPressupost()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(this);
        }
        private void Pressupost(object sender, EventArgs e)
        {
            Controlador.ObtenirPressupost();
            this.Navigation.PushModalAsync(new Pressupostos(BindingContext));
        }
        private void Menu(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Menu(BindingContext));
        }
        private void BuscarProductes(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new BuscarProductesPressupost(BindingContext));
        }
        private void BuscarClients(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new AgregarClientsPressupsot(BindingContext));
        }
    }
}