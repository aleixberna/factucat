﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactuCAT
{
    public static class LlistaProductes
    {
        public static List<string> Productes_Tiquets = new List<string>
        {

            
        };
        public static List<string> GetSearchResults(string queryString)
        {
            var normalizedQuery = queryString?.ToLower() ?? "";
            return Productes_Tiquets.Where(f => f.ToLowerInvariant().Contains(normalizedQuery)).ToList();
        }
    }
    
}
